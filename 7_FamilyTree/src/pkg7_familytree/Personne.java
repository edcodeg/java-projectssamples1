/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg7_familytree;

/**
 *
 * @author Benjamin
 */
public class Personne {
    public static int id = 0;
    private String nom;
    private String prenom;
    private String naissdate;
    private String naisslieu;
    private String decesdate;
    private String deceslieu;
    private Association assoparents;
    private Association assodescendance;
    
    Personne(String nom, String prenom,
	    String naissdate, String naisslieu,
	    String decesdate, String deceslieu) {
	this.nom = nom;
	this.prenom = prenom;
	this.naissdate = naissdate;
	this.naisslieu = naisslieu;
	this.decesdate = decesdate;
	this.deceslieu = deceslieu;
	this.id++;
    }
    
}
