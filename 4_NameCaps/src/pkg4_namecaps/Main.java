/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg4_namecaps;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Benjamin
 */
public class Main {
    
    // IN <= STRING
    // OUT => 0 if it's not a particule
    //     => 1 if it's a particule listed below
    //     => 2 if it's a mini particule, it means a letter then an apostrophe
    public static int isParticle(String _in) {
	String particles[] = {"de", "der", "des", "du", "la", "le", "von"};
	
	if (Arrays.asList(particles).contains(_in.toLowerCase())) return 1;
	else if (_in.length() > 1 && '\'' == _in.charAt(1)) return 2;
	else if (_in.contains("-")) return 3;
	else return 0;
    }
    
    // IN <= STRING
    // OUT => IN on lower case
    public static String formatParticle(String _in) {
	return _in.toLowerCase();
    }
    
    // IN <= STRING
    // OUT => take IN, put the first char to lower case,
    //	      then set the 3rd char to upper case
    public static String formatMiniParticle(String _in) {
	String out = _in.toLowerCase();
	out = out.substring(0, 2) + Character.toUpperCase(out.charAt(2)) + out.substring(3);
	return out;
    }
    
    public static String formatCompoundName(String _in) {
	String out = _in.toLowerCase();
	out =	Character.toUpperCase(out.charAt(0)) +
		out.substring(1, out.indexOf("-") + 1) +
		Character.toUpperCase(out.charAt(out.indexOf("-") + 1)) +
		out.substring(out.indexOf("-") + 2);
	return out;
    }
    
    // IN <= STRING
    // OUT => set the 1st char of IN to upper case
    public static String formatName(String _in) {
	String out = _in.toLowerCase();
	out = Character.toUpperCase(out.charAt(0)) + out.substring(1);
	return out;
    }
    
    public static void main(String[] args) {
	System.out.println("NAME CAPS PROJECT");
	System.out.println("=================\n");
	
	System.out.print("Enter your data and click ENTER :\n> ");
	Scanner myScan = new Scanner(System.in);
	String in_names = myScan.nextLine();
	myScan.close();
	
	String separated_names[] = in_names.split(" "); // Separate different name separated by SPACE
	int num = separated_names.length; // Memory
	
	// Little TEXT
	System.out.print("  > So there ");
	if (1 >= num) System.out.println("is " + num + " word to check...");
	else System.out.println("are " + num + " words to check..."); // Plural form ?
	
	System.out.println("  > LET'S DO THIS !!");
	
	String[] capitalized_names = new String[num]; // <== RESULT will be inside
	
	for (int i = 0; i < num; i++) { // while uncapitalized names available DO
	    int j = isParticle(separated_names[i]);
	    
	    switch (j) {
		case 0 :
		    capitalized_names[i] = formatName(separated_names[i]);
		    break;
		case 1 :
		    capitalized_names[i] = formatParticle(separated_names[i]);
		    break;
		case 2 :
		    capitalized_names[i] = formatMiniParticle(separated_names[i]);
		    break;
		case 3 :
		    capitalized_names[i] = formatCompoundName(separated_names[i]);
		    break;
		default :
		    System.out.println("Something bad happened...");
		    break;
	    }
	}
	
	// RESUME
	System.out.println("> DATA IN  >> " + in_names);
	System.out.print("> DATA OUT >> ");
	for (int i = 0; i < num; i++) {
	    System.out.print(capitalized_names[i] + " ");
	}
	System.out.println();
	System.out.println();
	
	
	// THE END
	System.out.println("PROGRAM TERMINATED...");
    }
    
}
