package pkg8_football;

public class Equipe {

	private final String nom;
	private final String ville;
	// point, buts, buts contre, performance, ...
	// private Collection<Match> matches;
	private final int numero;
	private Groupe groupe;

	public Equipe(String nom, String ville, int numero) {
		this.nom = nom;
		this.ville = ville;
		this.numero = numero;
	}

	public void rejoint(Groupe groupe) {
		this.groupe = groupe;
	}

	public void affiche() {
		// TODO
		System.out.println(numero + " " + nom + " " + ville + " groupe:" + groupe.getNumero());
	}

	public String getNom() {
		return this.nom;
	}
}
