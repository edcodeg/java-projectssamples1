/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg5_chronometre;


/**
 *
 * @author Benjamin
 */
public class Stopwatch {
    private long startTime = 0;
    private long endTime = 0;
    
    public Stopwatch() {
	this.startTime = System.currentTimeMillis();
    }
    
    public long getStartTime() {
	return this.startTime;
    }
    
    public long getEndTime() {
	return this.endTime;
    }
    
    public void start() {
	this.startTime = System.currentTimeMillis();
    }
    
    public void stop() {
	this.endTime = System.currentTimeMillis();
    }
    
    public long getElapsedTime() {
	return System.currentTimeMillis() - getStartTime();
    }
}
