/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg6_matrix;

/**
 *
 * @author Benjamin
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
	Matrix a = new Matrix(new double[][] {
	    {4, 0, 3, 2},
	    {1,-1, 2, 0},
	    {0, 1,-2, 0},
	    {1, 0, 3, 1},
	});
	
	Matrix aa = new Matrix(new double[][] {
	    {4, 0, 3, 2},
	    {1,-1, 2, 0},
	    {0, 1,-2, 0},
	});
	
	System.out.println("Start testing without problems");
	try {
	    double deta = a.determinant();
	    Matrix b = a.inverse();
	    Matrix bb = a.subMatrix(2, 2);
	    Matrix uni = a.multiply(b);
	    Matrix half = uni.multiply(0.5);
	    Matrix two = uni.add(uni);
	    Matrix c = a.multiply(two);
	    Matrix a1 = c.subtract(a);
	    Matrix a2 = a.transpose();
	    
	    System.out.println("A.DET = " + deta);
	    System.out.println("MATRIX A (base)\n" + a.toString());
	    System.out.println("MATRIX B (A.INVERT)\n" + b.toString());
	    System.out.println("MATRIX BB (A.SUBMAT(2,2))\n" + bb.toString());
	    System.out.println("MATRIX UNI (A.MULT(B))\n" + uni.toString());
	    System.out.println("MATRIX HALF (UNI.MULT(0.5))\n" + half.toString());
	    System.out.println("MATRIX TWO (UNI.ADD(UNI))\n" + two.toString());
	    System.out.println("MATRIC C (A.MULT(TWO))\n" + c.toString());
	    System.out.println("MATRIX A1 (C.SUB(A))\n" + a1.toString());
	    System.out.println("MATRIX A2 (A.TRANSP)\n" + a2.toString());
	    
	    System.out.println("Tests completed");
	    System.out.println();
	}
	
	catch (IllegalArgumentException ex) {
	    System.out.print("EX::");
	    System.out.println(ex.getMessage());
	}
	
	System.out.println("Start testing with problems");
	try {
	    Matrix testfail = new Matrix(0, 2);
	} catch (IllegalArgumentException ex) {
	    System.out.print("EX::");
	    System.out.println(ex.getMessage());
	}
	
	try {
	    double valuefail[][] = {};
	    Matrix testfail = new Matrix(valuefail);
	} catch (IllegalArgumentException ex) {
	    System.out.print("EX::");
	    System.out.println(ex.getMessage());
	}
	
	System.out.println("\nMATRIX AA (base)\n" + aa.toString());
	try {
	    Matrix b = aa.inverse();
	} catch (IllegalArgumentException ex) {
	    System.out.print("EX::");
	    System.out.println(ex.getMessage());
	}
	
	try {
	    Matrix uni = a.multiply(aa);
	} catch (IllegalArgumentException ex) {
	    System.out.print("EX::");
	    System.out.println(ex.getMessage());
	}
	
	try {
	    Matrix two = aa.add(a);
	} catch (IllegalArgumentException ex) {
	    System.out.print("EX::");
	    System.out.println(ex.getMessage());
	}
	
	try {
	    Matrix a1 = aa.subtract(a);
	} catch (IllegalArgumentException ex) {
	    System.out.print("EX::");
	    System.out.println(ex.getMessage());
	}
	
	try { // Impossible to not transpose 
	    Matrix a2 = aa.transpose();
	} catch (IllegalArgumentException ex) {
	    System.out.print("EX::");
	    System.out.println(ex.getMessage());
	}
	
	try {
	    Matrix a3 = aa.cofactor();
	} catch (IllegalArgumentException ex) {
	    System.out.print("EX::");
	    System.out.println(ex.getMessage());
	}
	
	try {
	    Matrix matrixfail = new Matrix(1, 2);
	    Matrix refail = matrixfail.subMatrix(1, 1);
	} catch (IllegalArgumentException ex) {
	    System.out.print("EX::");
	    System.out.println(ex.getMessage());
	}
	
	try {
	    Matrix matrixfail = new Matrix(3, 2);
	    Matrix refail = matrixfail.subMatrix(50, 100);
	} catch (IllegalArgumentException ex) {
	    System.out.print("EX::");
	    System.out.println(ex.getMessage());
	}
	
	System.out.println("Tests completed");
    }    
}
